# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Configuracion(models.Model):
    """Clase para la tabla de configuracion"""
    opcion = models.CharField(max_length=20)
    valor = models.DecimalField(max_digits=9, decimal_places=2,
                                blank=False, null=False)

    def __str__(self):
        """ Cadena que se retorna en las querys """
        return "%s: %f" % (self.opcion, self.valor)
