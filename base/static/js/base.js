$('#id_actualizar_conf').on('click', function() {
    var tasa_interes = parseFloat($('#id_tasa_interes').val());
    var porcentaje_enganche = parseFloat($('#id_porcentaje_enganche').val());


    if(!confirm('¿Seguro que desea continuar? Perderá el trabajo que este realizando')) {
        return 0;
    }

    $.ajax('/configurar/'+tasa_interes+'/'+porcentaje_enganche)
    .done(function(data){
        console.log(data);
        if (data == 1) {
            window.location.replace('/');
        }
    });
});