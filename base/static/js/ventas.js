var dproductos = [];
var tasa_interes = 0;
var porcentaje_enganche = 0;
var total_importe = 0;
var total_real = 0;

$.ajax('/datos').done(function(data){
    tasa_interes = data['tasa_interes']*.01;
    porcentaje_enganche = data['porcentaje_enganche']*.01;
});

$('#id_cliente').change(function() {
    var id_cliente = $('#id_cliente').val()
    //Si se selecciona un cliente
    if (id_cliente != 0) {
        // Si el id de cliente es diferente de 0 consultamos los datos
        $.ajax("../clientes/json/"+id_cliente)
        .done(function(data){
            var cliente = data[0]['fields'];
            //Se llenan los input para mostrar los datos del cliente
            $('#id_direccion').val(cliente['direccion']);
            $('#id_ciudad').val(cliente['ciudad']+", "+cliente['estado']);
            $('#id_rfc').val(cliente['rfc']);
        });
    }
});

$('#id_producto').change(function() {
    $('#id_cantidad').focus();
});

$('#id_agregar').on('click', function(e){
    e.preventDefault();
    agregar_producto();
});

var agregar_producto = function() {
    var ultimo_producto = $('#tabla').find('tr').last();
    var id_producto = $('#id_producto').val();
    var cantidad = parseInt($('#id_cantidad').val());

    if (!cantidad || cantidad < 1 || cantidad > 9999) {
        //Si no hay cantidad o es menor a 1 no se hace nada
        alert('Valor de cantidad invalido.');
        return 0;
    }

    //Checa si el el producto ya esta en la tabla, si existe no hace nada
    if (dproductos.indexOf(String(id_producto)) != -1) {
        alert('Este producto ya está agregado.');
        limpiar_productos();
        return 0
    }

    if (id_producto != "0") {
        $.ajax("../productos/json/"+id_producto)
        .done(function(data){
            var producto = data[0]['fields'];
            var precio_real = parseFloat(producto['precio']);
            var precio = precio_real + precio_real * tasa_interes * 12;
            var importe = parseFloat(cantidad)*precio
            ultimo_producto.after('<tr data-id="'+id_producto+'" '+
                'data-precio_real="'+precio_real+'" data-cantidad="'
                +cantidad+'" data-importe="'+importe+'"><td>'+
                producto.descripcion+'</td><td>'+
                producto.modelo+'</td><td>'+cantidad+'</td><td>'+
                precio.toFixed(2)+'</td><td>'+importe.toFixed(2)+'</td>'+
                '<td>'+
                '<i onclick="eliminar(this)" class="btn btn-xs btn-danger fa fa-minus"></i></td>');

            total_importe = total_importe + importe;
            total_real = total_real + precio_real * cantidad;
            calcular(false);
            tabla_abonos();
        });
    }

    dproductos.push(id_producto);
    

    limpiar_productos();
}

var limpiar_productos = function() {
    $('#id_producto').val("0");
    $('#id_cantidad').val("");
    $('#id_producto').focus();
}

var eliminar = function(o){
    $(o).parent().parent().remove();
    var id_producto =$(o).parent().parent().data('id');
    var cantidad =$(o).parent().parent().data('cantidad');
    var precio_real =$(o).parent().parent().data('precio_real');
    var importe =$(o).parent().parent().data('importe');
    total_real = total_real - precio_real * cantidad;
    total_importe = total_importe - importe;
    var index = dproductos.indexOf(String(id_producto));
    dproductos.splice(index, 1);
    calcular(false);
    tabla_abonos();
}

var calcular = function(cambio_enganche) {
    var intereses = total_real * tasa_interes;
    var total_intereses = total_importe + intereses;
    if (cambio_enganche) {
        var enganche = parseFloat($('#id_enganche').val());
    } else {
        var enganche = total_importe * porcentaje_enganche;
    }
    var total_adeudo = (total_real - enganche) * (tasa_interes * 12 + 1);
    var bonificacion = total_importe - enganche - total_adeudo;

    
    $('#id_enganche').val(enganche.toFixed(2));
    $('#id_bonificacion').val(bonificacion.toFixed(2));
    $('#id_adeudo').val(total_adeudo.toFixed(2));
}

$('#id_enganche').keyup(function(e) {
    if (e.keyCode == 13) {
        calcular(true);
        tabla_abonos();
    }
});

var tabla_abonos = function() {
    var total_adeudo = parseFloat($('#id_adeudo').val());
    var enganche = parseFloat($('#id_enganche').val());
    if (total_importe > 0) {
        var html = ``;
        [3,6,9,12].forEach(function(n) {
            adeudo = (total_real - enganche) * (tasa_interes * n + 1);
            html = html + `
                <tr>
                <tr>
                    <td>${n} abonos de $</td>
                    <td>${parseFloat(adeudo/n).toFixed(2)}</td>
                    <td>Total a pagar $</td>
                    <td>${adeudo.toFixed(2)}</td>
                    <td>Se ahorra $</td>
                    <td>${(total_adeudo-adeudo).toFixed(2)}</td>
                </tr>
            `
        });
        $('#tabla_abonos').html(html);
    } else {
        $('#tabla_abonos').html('');
    }
}