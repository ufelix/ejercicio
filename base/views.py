# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.http import JsonResponse, HttpResponse


from models import Configuracion


def configuracion():
    """ Regresa un diccionario con los valores de la configuracion """
    configuracion = Configuracion.objects.all()
    return {c.opcion: float(c.valor) for c in configuracion}


def configurar(request, tasa_interes, porcentaje_enganche):
    """ Vista para guardar la configuracion en la base de datos """
    otasa_interes = Configuracion.objects.get(opcion='tasa_interes')
    oporcentaje_enganche = Configuracion.objects.get(opcion='porcentaje_enganche')

    try:
        otasa_interes.valor = float(tasa_interes)
        oporcentaje_enganche.valor = float(porcentaje_enganche)
        otasa_interes.save()
        oporcentaje_enganche.save()
    except Exception:
        return HttpResponse(0)

    return HttpResponse(1)


def index(request):
    """Vista inicial"""
    return render_to_response("base/base.html", configuracion())


def datos_configuracion(request):
    """ Regresa los datos de la configuración en formato JSON """
    return JsonResponse(configuracion())
