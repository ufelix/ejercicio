# -*- coding: utf-8 -*-
from django.forms import ModelForm

from models import Cliente


class ClienteForm(ModelForm):
    """ Formulario de cliente, utiliza el modelo creado para crear el formulario automaticamente """
    class Meta:
        model = Cliente
        exclude = []
