# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Cliente(models.Model):
    """Clase Cliente con los campos de su tabla"""
    nombre = models.CharField(u"Nombre", max_length=200, blank=False, null=False)
    direccion = models.CharField(u"Dirección", max_length=300, blank=False, null=False)
    ciudad = models.CharField(u"Ciudad", max_length=50, blank=False, null=False)
    estado = models.CharField(u"Estado", max_length=50, blank=False, null=False)
    rfc = models.CharField(u"RFC", max_length=13, blank=False, null=False)

    def __str__(self):
        """ Cadena que se muestra en las querys  """
        return self.nombre
