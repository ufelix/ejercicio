from django.conf.urls import url

import views

# Conexion de los URLs con las funciones de las vistas
urlpatterns = [
    url(r'^$', views.lista),
    url(r'agregar$', views.agregar_editar),
    url(r'editar/([0-9]+)$', views.agregar_editar),
    url(r'eliminar/([0-9]+)$', views.eliminar),
    url(r'json/([0-9]+)$', views.cliente_json),
]
