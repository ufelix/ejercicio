from django.shortcuts import render_to_response, redirect
from django.template.context_processors import csrf
from django.core import serializers
from django.http import HttpResponse

from base.views import configuracion

from models import Cliente
from forms import ClienteForm


def lista(request):
    """ """
    contexto = {'opcion': 'Clientes'} # Diccionario que se pasa a la plantilla con las variables que se necesiten
    contexto['clientes'] = Cliente.objects.all() # Query con la lista de empleados
    contexto.update(configuracion())

    return render_to_response("clientes/lista.html", contexto)


def agregar_editar(request, id_cliente=0):
    """ Vista para agregar y editar clientes """
    # Si se recibe el id del cliente en la URL hacemos la query para recibir el empleado y la pasamos al contexto
    if id_cliente:
        contexto = {'opcion': 'Editar Cliente'}
        cliente = Cliente.objects.get(id=id_cliente)
        contexto['cliente'] = cliente
    # Si no se recibe el id del cliente inicializamos la variable cliente ya que es usada mas adelante
    else:
        contexto = {'opcion': 'Agregar Cliente'}
        cliente = None

    contexto.update(csrf(request)) # Variable que utiliza django para proteger el sitio de ataques Cross-site

    formulario = ClienteForm(instance=cliente) # Creamos el formulario
    # Se maneja la peticion del formulario al agregar o editar
    if request.method == 'POST':
        # Se crea el formulario pasando como variables los datos del formulario y el objeto empleado
        formulario = ClienteForm(request.POST or None, instance=cliente)
        # Si el formulario es valido lo guardamos y redireccionamos a la lista de empleados
        if formulario.is_valid():
            formulario.save()
            return redirect('/clientes')
        # Si el formulario no es valido creamos el formulario pasando como variable los datos del formulario
        # para mostrar los errores
        else:
            formulario = ClienteForm(request.POST or None)

    # Se guarda en el contexto el formulario
    contexto['formulario'] = formulario

    return render_to_response("clientes/agregar_editar.html", contexto)


def eliminar(request, id_cliente=0):
    """ Funcion para eliminar un empleado """
    if id_cliente:
        cliente = Cliente.objects.get(id=id_cliente) #Se obtiene el empleado
        cliente.delete() # Se elimina
        return redirect('/clientes') # Redireccion a la lista de empleados


def cliente_json(request, id_cliente):
    """ Retorna el cliente en formato json """
    cliente = Cliente.objects.filter(id=id_cliente)
    data = serializers.serialize('json', cliente)
    return HttpResponse(data, content_type='application/json')
