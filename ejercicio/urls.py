from django.conf.urls import url, include

from base.views import index, configurar, datos_configuracion

# Conexion de las URLs con las funciones de las vistas
urlpatterns = [
    url(r'^$', index),
    url(r'^datos/$', datos_configuracion),
    url(r'configurar/' +
        '(?P<tasa_interes>([0-9]+\.)?[0-9]+)/' +
        '(?P<porcentaje_enganche>([0-9]+\.)?[0-9]+)$',
        configurar),
    url(r'^clientes/', include('clientes.urls')),
    url(r'^productos/', include('productos.urls')),
    url(r'^ventas/', include('ventas.urls')),
]
