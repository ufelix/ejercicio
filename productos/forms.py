# -*- coding: utf-8 -*-
from django.forms import ModelForm

from models import Producto


class ProductoForm(ModelForm):
    """ Formulario de producto, utiliza el modelo creado para crear el formulario
    automaticamente """
    class Meta:
        model = Producto
        exclude = []
