# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Producto(models.Model):
    """Clase producto con los campos de su tabla"""
    descripcion = models.CharField(u"Descripción", max_length=200, blank=False,
                                   null=False)
    modelo = models.CharField(u"Modelo", max_length=300, blank=False,
                              null=False)
    precio = models.DecimalField("Precio", max_digits=9, decimal_places=2,
                                 blank=False, null=False)

    def __str__(self):
        """ Cadena que se muestra en las querys  """
        return u'%s (%s)' % (self.descripcion, self.modelo)
