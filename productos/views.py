from django.shortcuts import render_to_response, redirect
from django.template.context_processors import csrf
from django.core import serializers
from django.http import HttpResponse

from models import Producto
from forms import ProductoForm


def lista(request):
    """ """
    contexto = {'opcion': 'Productos'} # Diccionario que se pasa a la plantilla con las variables que se necesiten
    contexto['productos'] = Producto.objects.all() # Query con la lista de empleados

    return render_to_response("productos/lista.html", contexto)


def agregar_editar(request, id_producto=0):
    """ Vista para agregar y editar productos """
    # Si se recibe el id del producto en la URL hacemos la query para recibir el empleado y la pasamos al contexto
    if id_producto:
        contexto = {'opcion': 'Editar Producto'}
        producto = Producto.objects.get(id=id_producto)
        contexto['producto'] = producto
    # Si no se recibe el id del producto inicializamos la variable producto ya que es usada mas adelante
    else:
        contexto = {'opcion': 'Agregar Producto'}
        producto = None

    contexto.update(csrf(request)) # Variable que utiliza django para proteger el sitio de ataques Cross-site

    formulario = ProductoForm(instance=producto) # Creamos el formulario
    # Se maneja la peticion del formulario al agregar o editar
    if request.method == 'POST':
        # Se crea el formulario pasando como variables los datos del formulario y el objeto empleado
        formulario = ProductoForm(request.POST or None, instance=producto)
        # Si el formulario es valido lo guardamos y redireccionamos a la lista de empleados
        if formulario.is_valid():
            formulario.save()
            return redirect('/productos')
        # Si el formulario no es valido creamos el formulario pasando como variable los datos del formulario
        # para mostrar los errores
        else:
            formulario = ProductoForm(request.POST or None)

    # Se guarda en el contexto el formulario
    contexto['formulario'] = formulario

    return render_to_response("productos/agregar_editar.html", contexto)


def eliminar(request, id_producto=0):
    """ Funcion para eliminar un empleado """
    if id_producto:
        producto = Producto.objects.get(id=id_producto) #Se obtiene el empleado
        producto.delete() # Se elimina
        return redirect('/productos') # Redireccion a la lista de empleados


def producto_json(request, id_producto):
    """ Retorna el producto en formato json """
    producto = Producto.objects.filter(id=id_producto)
    data = serializers.serialize('json', producto)
    return HttpResponse(data, content_type='application/json')
