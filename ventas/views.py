from django.shortcuts import render_to_response, redirect
from django.template.context_processors import csrf
from django.core import serializers
from django.http import HttpResponse

from clientes.models import Cliente
from productos.models import Producto
from base.views import configuracion


def venta(request):
    """ Vista para hacer las ventas """
    # Diccionario que se pasa a la plantilla con las variables que se necesiten
    contexto = {'opcion': 'Ventas'}
    contexto.update(csrf(request))
    contexto.update(configuracion())
    # Query con la lista de empleados
    contexto['clientes'] = Cliente.objects.all()
    contexto['productos'] = Producto.objects.all()

    return render_to_response("ventas/venta.html", contexto)
